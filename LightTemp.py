#!/usr/bin/env python3
#############################################################################
# Filename    : LightTemp.py
# Location    : ~/home
#
# Description : This program uses a photoresistor and a thermistor 
#               to implement a thermometer and light detector.  The
#               implementation uses a Raspberry Pi Zero.  The program
#               returns the current temperature and light level at a 
#               frequency specified in the `loop()` function.
# Running     : Run from cron by editing crontab -e include line:
#   		* * * * * /home/LightTemp.py
#
# Author      : Bjorn Melin, Joseph Hentges
# modification: 02/12/2020
#############################################################################
import RPi.GPIO as GPIO
import smbus
import time
import math
from datetime import datetime

address = 0x48		# pcf8591 default address
bus=smbus.SMBus(1)
cmd=0x40		# command

# read ADC value
# @param chn - channel 0, 1, 2, or 3
def analogRead(chn):
	value = bus.read_byte_data(address,cmd+chn)
	return value
	
# write DAC value
# @param value - value read from the ADC
def analogWrite(value):
	bus.write_byte_data(address,cmd,value)	

# initialize the GPIO board	
def setup():
	GPIO.setmode(GPIO.BOARD)
	
# running of the program
def run():
	time.sleep(1)
	now = str(datetime.now().strftime('%Y/%m/%d %H:%M'))	# time of execution
	temp_file = open("/home/pi/data/temp_data.txt", "a+")
	light_file = open("/home/pi/data/light_data.txt", "a+")

	#### Thermistor Sensor Readings ####
	value = analogRead(0)			# read ADC value of channel 0. A0 pin
	voltage = value / 255.0 * 3.3		# calculate voltage
	Rt = 10 * voltage / (3.3 - voltage)	# calculate resistance value of thermistor
	tempK = 1/(1/(273.15 + 25) + math.log(Rt/10)/3950.0) # calculate temperature (Kelvin)
	tempC = tempK -273.15			# calculate temperature (Celsius)
	tempF = (tempC * 9.0/5.0) + 32		# calculate temperature (Fahrenheit)
	# print out the temp results from the themistor
	print ('%s, ADC Temp Value :  %d, Voltage : %.2f, Temperature : %.2f'%(now,value,voltage,tempF))
	# write out the temp results to a file `temp_file.txt` including date & time
	temp_file.write('%s,%.2f,%.2f\n'%(now,voltage,tempF))
	time.sleep(0.5)				# sleep for 0.5 seconds

	#### Photoresistor Sensor Readings ####
	value = analogRead(3)    		# read the ADC value of channel 3. A3 pin
	voltage = value / 255.0 * 3.3		# calculate voltage
	# print out the light results from the photoresistor
	print ('%s, ADC Light Value : %d, Voltage : %.2f\n'%(now,value,voltage))
	# write out the light results to a file `light_file.txt' including date & time
	light_file.write('%s,%.2f,%d\n'%(now,voltage,value))
	time.sleep(0.5)				# sleep for 0.5 seconds
	

def destroy():
	GPIO.cleanup()
	
if __name__ == '__main__':  # Program entrance
	print ('Program is starting ... ')
	setup()
	try:
		run()
	except KeyboardInterrupt: # Press ctrl-c to end the program.
		destroy()
	
