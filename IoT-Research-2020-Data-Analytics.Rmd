---
title: "IoT Research 2020 - Data Analytics"
author: "Bjorn Melin and Joseph Hentges"
date: "3/17/2020"
output: html_document
---

```{r, message=FALSE, error=FALSE, warning=FALSE, echo=FALSE, include=FALSE}
library(tidyverse)
library(ggplot2)
library(stringr)
library(tidyr)
library(lubridate)
```



```{r, warning=FALSE}
# Read TAB delimited files
light_data_1 <- read.delim("/Users/bjornmelin/Documents/Bjorn/School/Computer\ Science/Research/IoT/Code/Data/3-18-2020-light_data.txt", header = FALSE, sep = ",", dec = ".")

temp_data_1 <- read.delim("/Users/bjornmelin/Documents/Bjorn/School/Computer\ Science/Research/IoT/Code/Data/3-18-2020-temp_data.txt", header = FALSE, sep = ",", dec = ".")

light_data_2 <- read.delim("/Users/bjornmelin/Documents/Bjorn/School/Computer\ Science/Research/IoT/Code/Data/3-24-2020-light_data_1.txt", header = FALSE, sep = ",", dec = ".")

temp_data_2 <- read.delim("/Users/bjornmelin/Documents/Bjorn/School/Computer\ Science/Research/IoT/Code/Data/3-24-2020-temp_data_1.txt", header = FALSE, sep = ",", dec = ".")

light_data_3 <- read.delim("/Users/bjornmelin/Documents/Bjorn/School/Computer\ Science/Research/IoT/Code/Data/3-24-2020-light_data_2.txt", header = FALSE, sep = ",", dec = ".")

temp_data_3 <- read.delim("/Users/bjornmelin/Documents/Bjorn/School/Computer\ Science/Research/IoT/Code/Data/3-24-2020-temp_data_2.txt", header = FALSE, sep = ",", dec = ".")

# remove data collected before Pi's were in correct positions
light_data_1 <- light_data_1[-c(1:47),]
temp_data_1 <- temp_data_1[-c(1:47),]

light_data_2 <- light_data_2[-c(1:12),]
temp_data_2 <- temp_data_2[-c(1:12),]

light_data_3_2 <- light_data_3[-c(1:12),]
temp_data_3_2 <- temp_data_3[-c(1:12),]


light_data_1 <- light_data_1[c(rep(FALSE,14),TRUE), ]
light_data_2 <- light_data_2[c(rep(FALSE,14),TRUE), ]
light_data_sens2 <- light_data_3_2[c(rep(FALSE,14),TRUE), ]

temp_data_1 <- temp_data_1[c(rep(FALSE,14),TRUE), ]
temp_data_2 <- temp_data_2[c(rep(FALSE,14),TRUE), ]
temp_data_sens2 <- temp_data_3_2[c(rep(FALSE,14),TRUE), ]

# combine all data collected into a single dataframe
light_data_sens1_1 <- light_data_1
light_data_sens1_2 <- light_data_2

temp_data_sens1_1 <- temp_data_1
temp_data_sens1_2 <- temp_data_2


names(light_data_sens1_1) <- c("Timestamp", "Voltage", "ADC_Light_Value")
names(temp_data_sens1_1) <- c("Timestamp", "Voltage", "Temp_Fahr")

names(light_data_sens1_2) <- c("Timestamp", "Voltage", "ADC_Light_Value")
names(temp_data_sens1_2) <- c("Timestamp", "Voltage", "Temp_Fahr")

names(light_data_sens2) <- c("Timestamp", "Voltage", "ADC_Light_Value")
names(temp_data_sens2) <- c("Timestamp", "Voltage", "Temp_Fahr")
```



```{r}
light_data_sens1_1 <- light_data_sens1_1 %>% 
  separate(Timestamp, into = c("Date", "Time"), sep = " ")

light_data_sens1_2 <- light_data_sens1_2 %>% 
  separate(Timestamp, into = c("Date", "Time"), sep = " ")

temp_data_sens1_1 <- temp_data_sens1_1 %>% 
  separate(Timestamp, into = c("Date", "Time"), sep = " ")

temp_data_sens1_2 <- temp_data_sens1_2 %>% 
  separate(Timestamp, into = c("Date", "Time"), sep = " ")

light_data_sens2 <- light_data_sens2 %>% 
  separate(Timestamp, into = c("Date", "Time"), sep = " ")

temp_data_sens2 <- temp_data_sens2 %>% 
  separate(Timestamp, into = c("Date", "Time"), sep = " ")
```


```{r}
temp  <-function(data)  {
  counter <- 0
  newList <- {}
  for (i in data) {
    if (counter %% 4  == 0) {
      newList[counter] = data[counter]
    }else {
      newList[counter] =''
    }
    counter = counter + 1
  }
  newList[counter] = ""
  return (newList)
}


# Sensor 1
time1 <- temp_data_sens1_1
temp_data_sens1_1$xlab <- temp(time1$Time)

time2 <- temp_data_sens1_2
temp_data_sens1_2$xlab <- temp(time2$Time)

time3 <- light_data_sens1_1
light_data_sens1_1$xlab <- temp(time3$Time)

time4 <- light_data_sens1_2
light_data_sens1_2$xlab <- temp(time4$Time)


# Sensor 2
time5 <- temp_data_sens2
temp_data_sens2$xlab <- temp(time5$Time)

time6 <- light_data_sens2
light_data_sens2$xlab <- temp(time6$Time)
```



```{r}
ggplot(data = temp_data_sens1_1, aes(x = Time, y = Temp_Fahr)) + 
  geom_line(aes(group=1)) + 
  facet_grid("Date") +
  xlab("Time") + 
  ylab("Temperature (Fahrenheit)") + 
  ggtitle("Sensor 1: Temperature Levels", subtitle = "3/17/2020 - 3/18/2020") +
  scale_x_discrete(breaks = temp_data_sens1_1$xlab) +
  theme_bw() +
  theme(axis.title = element_text(face = "bold", size = rel(1)),
        plot.title = element_text(size = rel(1.2), face = "bold", hjust = 0.5),
        plot.subtitle = element_text(size = rel(1), hjust = 0.5),
        axis.text.x = element_text(size = 8, angle = 45, face = "bold", vjust = 0.5),
        axis.text.y = element_text(size = 8, face = "bold")) 
```


```{r}
ggplot(data = temp_data_sens1_2, aes(x = Time, y = Temp_Fahr)) + 
  geom_line(aes(group=1)) + 
  facet_grid("Date") +
  xlab("Time") + 
  ylab("Temperature (Fahrenheit)") + 
  ggtitle("Sensor 1: Temperature Levels", subtitle = "3/24/2020 - 3/25/2020") +
  scale_x_discrete(breaks = temp_data_sens1_2$xlab) +
  theme_bw() +
  theme(axis.title = element_text(face = "bold", size = rel(1)),
        plot.title = element_text(size = rel(1.2), face = "bold", hjust = 0.5),
        plot.subtitle = element_text(size = rel(1), hjust = 0.5),
        axis.text.x = element_text(size = 8, angle = 45, face = "bold", vjust = 0.5),
        axis.text.y = element_text(size = 8, face = "bold")) 
```


```{r}
ggplot(data = light_data_sens1_1, aes(x = Time, y = ADC_Light_Value)) + 
  geom_line(aes(group=1)) + 
  facet_grid("Date") +
  xlab("Time") + 
  ylab("ADC Light Value") + 
  ggtitle("Sensor 1: Ambient Light Levels", subtitle = "3/17/2020 - 3/18/2020") +
  scale_x_discrete(breaks = light_data_sens1_1$xlab) +
  theme_bw() +
  theme(axis.title = element_text(face = "bold", size = rel(1)),
        plot.title = element_text(size = rel(1.2), face = "bold", hjust = 0.5),
        plot.subtitle = element_text(size = rel(1), hjust = 0.5),
        axis.text.x = element_text(size = 8, angle = 45, face = "bold", vjust = 0.5),
        axis.text.y = element_text(size = 8, face = "bold")) 
```


```{r}
ggplot(data = light_data_sens1_2, aes(x = Time, y = ADC_Light_Value)) + 
  geom_line(aes(group=1)) + 
  facet_grid("Date") +
  xlab("Time") + 
  ylab("ADC Light Value") + 
  ggtitle("Sensor 1: Ambient Light Levels", subtitle = "3/24/2020 - 3/25/2020") +
  scale_x_discrete(breaks = light_data_sens1_2$xlab) +
  theme_bw() +
  theme(axis.title = element_text(face = "bold", size = rel(1)),
        plot.title = element_text(size = rel(1.2), face = "bold", hjust = 0.5),
        plot.subtitle = element_text(size = rel(1), hjust = 0.5),
        axis.text.x = element_text(size = 8, angle = 45, face = "bold", vjust = 0.5),
        axis.text.y = element_text(size = 8, face = "bold")) 
```


```{r}
#summarize by 10 mins for each
ggplot(data = temp_data_sens2, aes(x = Time, y = Temp_Fahr)) + 
  geom_line(aes(group=1)) + 
  facet_grid("Date") +
  xlab("Time") + 
  ylab("Temperature (Fahrenheit)") + 
  ggtitle("Sensor 2: Temperature Levels") +
  scale_x_discrete(breaks = temp_data_sens2$xlab) +
  theme_bw() +
  theme(axis.title = element_text(face = "bold", size = rel(1)),
        plot.title = element_text(size = rel(1.2), face = "bold", hjust = 0.5),
        axis.text.x = element_text(size = 8, angle = 45, face = "bold", vjust = 0.5),
        axis.text.y = element_text(size = 8, face = "bold")) 
```




```{r}
#summarize by 10 mins for each
ggplot(data = light_data_sens2, aes(x = Time, y = ADC_Light_Value)) + 
  geom_line(aes(group=1)) + 
  facet_grid("Date") + 
  xlab("Time") + 
  ylab("ADC Light Value") + 
  ggtitle("Sensor 2: Ambient Light Levels") +
  scale_x_discrete(breaks = light_data_sens2$xlab) +
  theme_bw() +
  theme(axis.title = element_text(face = "bold", size = rel(1)),
        plot.title = element_text(size = rel(1.2), face = "bold", hjust = 0.5),
        axis.text.x = element_text(size = 8, angle = 45, face = "bold", vjust = 0.5),
        axis.text.y = element_text(size = 8, face = "bold")) 
```



